<?php
namespace Conicyt;


class Log
{

    private $appName;

    function __construct($appName)
    {
        $this->appName = $appName;
    }

    function info($event, $values = [])
    {
        Log::coniLog($this->appName, $event, $values, LOG_INFO);
    }

    function error($event, $values = [])
    {
        Log::coniLog($this->appName, $event, $values, LOG_ERR);
    }

    private static function coniLog($app, $event, $values, $level)
    {
        $message = [
            "app" => $app,
            "event" => $event
        ];

        if (array_key_exists('user', $values)) {
            $message["user"] = $values['user'];
            unset($values['user']);
        }

        if (array_key_exists('userip', $values)) {
            $message["userip"] = $values['userip'];
            unset($values['userip']);
        }

        if (array_key_exists('context', $values)) {
            $message["context"] = $values['context'];
            unset($values['context']);
        }

        if(count($values)) {
            $message["message"] = json_encode($values, JSON_UNESCAPED_SLASHES);
        }
        else {
            $message["message"] = "{}";
        }

        syslog($level | LOG_LOCAL0, json_encode($message, JSON_UNESCAPED_SLASHES));
    }
}