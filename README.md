Log Library
---
Funciones común de logs hacia syslog

Uso
---
```
cat << EOF > composer.json
{
    "repositories": [
        {
            "type": "vcs",
            "url": "https://bitbucket.org/conicytdev/conicyt-log-library"
        }
    ],
    "require": {
        "conicyt/log-library": "0.0.10"
    }
}
EOF

composer install
```

Ejemplo
---

```java

require_once __DIR__ . "/vendor/autoload.php";

// "my-app" es el nombre de la aplicacion
$logger = new Conicyt\Log("my-app");

// "loginExitoso" es el nombre del evento
$logger->info("loginExitoso", ["user" => "anunez@conicyt.cl", "userip" => "192.168.210.145", "campo1" => "valor1", "campo2" => "valor2"]);

// "parsingError" es el nombre del evento
$logger->error("parsingError", ["file" => __FILE__, "line" => __LINE__]);
```